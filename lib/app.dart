import 'package:flutter/material.dart';
import 'package:flutter_pangram/pages/home/home_page.dart';
import 'common/router/router.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      routes: Router.routes,
      onGenerateRoute: Router.onGenerateRoute,
      onUnknownRoute: Router.onUnknownRoute,
      theme: ThemeData(
      ),
      home: MyHomePage(),
    );
  }
}
