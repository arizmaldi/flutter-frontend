import 'dart:convert';
import 'package:flutter_pangram/domain/pangram_guttenberg/entities/book_entity.dart';
import 'package:meta/meta.dart';

class PangramGuttenberg extends BookGuttenbergEntity {
  final String id;
  final String title;
  final String author;
  final String poster;
  final String pangram;

  PangramGuttenberg({
    this.id,
    this.poster,
    @required this.title,
    @required this.author,
    this.pangram,
  });

  factory PangramGuttenberg.fromJson(Map<String, dynamic> json) {
    return PangramGuttenberg(
      id: json['_id'],
      title: json['title'],
      author: json['author'],
      poster: json['poster'],
      pangram: json['pangram'],
    );
  }
  Map<String, dynamic> toJson() {
    return {
      '_id': id,
      'title': title,
      'author': author,
      'poster': poster,
      'pangram': pangram
    };
  }

  @override
  String toString() {
    return jsonEncode(toJson());
  }
}

extension PangramGuttenbergExt on PangramGuttenberg {
  PangramGuttenberg copyWith(
          {String id,
          String title,
          String author,
          String poster,
          String pangram}) =>
      PangramGuttenberg(
          id: id ?? this.id,
          title: title ?? this.title,
          author: author ?? this.author,
          poster: poster ?? this.poster,
          pangram: pangram ?? this.pangram);
}
