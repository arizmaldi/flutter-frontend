import 'dart:convert';
import 'package:flutter_pangram/common/network/pangram_client.dart';
import 'package:flutter_pangram/data/pangram_list/models/pangram_guttenberg.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

import 'package:http/http.dart' as http;

@Bind.toType(PangramRemoteDataSourceImpl)
@injectable
abstract class PangramRemoteDataSource {
  Future<PangramGuttenberg> getRandomPangram();
  Future<PangramGuttenberg> getAllPangram();
}

@lazySingleton
@injectable
class PangramRemoteDataSourceImpl implements PangramRemoteDataSource {
  final PangramClient client;

  PangramRemoteDataSourceImpl({@required this.client});

  factory PangramRemoteDataSourceImpl.create() {
    return PangramRemoteDataSourceImpl(
      client: PangramClientImpl(http.Client()),
    );
  }

  @override
  Future<PangramGuttenberg> getRandomPangram() async {
    Uri uri = Uri.https(
      'hipi-backend.herokuapp.com',
      '/pangram',
    );
    final response = await client.get(uri);

    String json = response.body;
    // var pangram;
    // List<PangramGuttenberg> pangram = [];
    Map<String, dynamic> decodedJson = jsonDecode(json);
    // PangramGuttenberg pangram = PangramGuttenberg.fromJson(decodedJson);
    // print(pangram.author);
    return PangramGuttenberg.fromJson(decodedJson);
  }

  @override
  Future<PangramGuttenberg> getAllPangram() async {
    Uri uri = Uri.https(
      'hipi-backend.herokuapp.com',
      '/list',
    );
    final response = await client.get(uri);

    String json = response.body;
    Map<String, dynamic> decodedJson = jsonDecode(json);
    // PangramGuttenberg pangram = new PangramGuttenberg.fromJson(decodedJson);
    return PangramGuttenberg.fromJson(decodedJson);
  }
}
