import 'package:flutter/material.dart';
import 'package:flutter_pangram/common/errors/no_connection_error.dart';
import 'package:flutter_pangram/common/network/network_check.dart';
import 'package:flutter_pangram/data/pangram_list/datasources/pangram_remote_datasource.dart';
import 'package:flutter_pangram/domain/pangram_guttenberg/entities/book_entity.dart';
import 'package:flutter_pangram/domain/pangram_guttenberg/repository/book_repository.dart';
import 'package:injectable/injectable.dart';

@lazySingleton
@injectable
class PangramBookRepositoryImpl implements BookRepository {
  final PangramRemoteDataSource pangramBookNetworkDatasource;
  final NetworkCheck networkCheck;

  PangramBookRepositoryImpl({
    @required this.pangramBookNetworkDatasource,
    @required this.networkCheck,
  });

    @override
  Future<BookGuttenbergEntity> getAllPangram() async {
    return await pangramBookNetworkDatasource.getAllPangram();
  }

  @override
  Future<BookGuttenbergEntity> getRandomPangram() async {
    return await pangramBookNetworkDatasource.getRandomPangram();
  }
}
