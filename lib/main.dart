import 'package:flutter/material.dart';
import 'package:flutter_pangram/common/config/log_config.dart';

import 'app.dart' as app;
import 'common/config/injector.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await setupInjections();
  setupLogger();
  app.main();
}

