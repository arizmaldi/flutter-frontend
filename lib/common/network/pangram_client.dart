import 'package:flutter_pangram/common/errors/pangram_api_error.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@Bind.toType(PangramClientImpl)
@injectable
abstract class PangramClient extends BaseClient {}

typedef _PendingReuqest = Future<Response> Function();

@lazySingleton
@injectable
class PangramClientImpl extends PangramClient {
  final Client _client;

  PangramClientImpl(@required this._client);

  Future<StreamedResponse> send(BaseRequest request) {
    return _client.send(request);
  }

  @override
  Future<Response> get(url, {Map<String, String> headers}) {
    return _handleResponse(() => super.get(url, headers: headers));
  }

  Future<Response> _handleResponse(_PendingReuqest request) async {
    Response response = await request();
    if (response.statusCode >= 200 && response.statusCode < 300) {
      return response;
    }

    throw PangramApiError(
      uri: response.request.url,
      code: response.statusCode,
      body: response.body,
      method: response.request.method,
    );
  }
}
