abstract class Routes {
  static const String homePath = 'home';
  static const String home = '/$homePath';
  static const String dashboardPath = 'dashboard';
  static const String dashboard = '$home/$dashboardPath';
  static const String anagramPath = 'anagram';
  static const String anagram = '$home/$anagramPath';
  static const String isogramPath = 'isogram';
  static const String isogram = '$home/$isogramPath';
  static const String notFoundPath = 'notFound';
  static const String notFound = '/$notFoundPath';
}
