import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pangram/common/config/injector.dart';
import 'package:flutter_pangram/common/router/routes.dart';
import 'package:flutter_pangram/pages/anagram/anagram_page.dart';
import 'package:flutter_pangram/pages/home/home_page.dart';
import 'package:flutter_pangram/pages/isogram/isogram_page.dart';
import 'package:flutter_pangram/pages/notFound/not_found_page.dart';
import 'package:flutter_pangram/pages/pangram/pangram_page.dart';

abstract class Router {
  static Map<String, WidgetBuilder> routes = {
    Routes.home: (BuildContext context) => MyHomePage(),
    Routes.dashboard: (BuildContext context) => MyPangramPage(),
    Routes.anagram: (BuildContext context) => MyAnagramPage(),
    Routes.isogram: (BuildContext context) => MyIsogramPage(),
  };

    static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    final List<String> pathElements = settings.name.split('/');
    if (pathElements[0] != '') {
      return null;
    } else if (pathElements[1] == Routes.anagramPath) {
      return MaterialPageRoute(
        builder: (context) {
          return MyHomePage();
        },
      );
    }
    return null;
  }

  static Route<dynamic> onUnknownRoute(RouteSettings settings) {
    return MaterialPageRoute(
      builder: (context) {
        return MyNotFoundPage();
      },
    );
  }
}
