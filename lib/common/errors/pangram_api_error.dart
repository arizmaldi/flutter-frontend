import 'dart:io';

class PangramApiError extends HttpException {
  final int code;
  final String method;
  final String body;

  PangramApiError({
    String message = 'PangramApiError',
    this.method,
    Uri uri,
    this.body,
    this.code,
  }) : super(message, uri: uri);

  String toString() {
    var b = new StringBuffer()..write('PangramApiError: ')..write(message);
    if (method != null) {
      b.write(', method = $method');
    }
    if (uri != null) {
      b.write(', uri = $uri');
    }
    if (body != null) {
      b.write(', body = $body');
    }
    return b.toString();
  }
}
