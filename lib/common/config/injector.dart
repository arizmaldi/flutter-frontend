import 'package:connectivity/connectivity.dart';
import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'injector.iconfig.dart' as injection_config;
import 'package:http/http.dart' as http;


final getIt = GetIt.instance;
Future<void> setupInjections() async {

  getIt.registerLazySingleton(() => Connectivity());
  getIt.registerLazySingleton(() => http.Client());
  configure();
}

@injectableInit
void configure() => injection_config.$initGetIt();