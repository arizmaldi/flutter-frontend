// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:flutter_pangram/common/network/network_check.dart';
import 'package:connectivity/connectivity.dart';
import 'package:flutter_pangram/common/network/pangram_client.dart';
import 'package:http/src/client.dart';
import 'package:flutter_pangram/data/pangram_list/datasources/pangram_remote_datasource.dart';
import 'package:flutter_pangram/data/pangram_list/repositories/pangram_repository.dart';
import 'package:flutter_pangram/domain/pangram_guttenberg/repository/book_repository.dart';
import 'package:flutter_pangram/domain/pangram_guttenberg/usecase/book_usecase.dart';
import 'package:flutter_pangram/pages/pangram/bloc/pangram_bloc.dart';
import 'package:get_it/get_it.dart';

final getIt = GetIt.instance;
void $initGetIt({String environment}) {
  getIt
    ..registerFactory<NetworkCheck>(
        () => NetworkCheckImpl(connectivity: getIt<Connectivity>()))
    ..registerLazySingleton<NetworkCheckImpl>(
        () => NetworkCheckImpl(connectivity: getIt<Connectivity>()))
    ..registerFactory<PangramClient>(() => PangramClientImpl(getIt<Client>()))
    ..registerLazySingleton<PangramClientImpl>(
        () => PangramClientImpl(getIt<Client>()))
    ..registerFactory<PangramRemoteDataSource>(
        () => PangramRemoteDataSourceImpl(client: getIt<PangramClient>()))
    ..registerLazySingleton<PangramRemoteDataSourceImpl>(
        () => PangramRemoteDataSourceImpl(client: getIt<PangramClient>()))
    ..registerLazySingleton<PangramBookRepositoryImpl>(() =>
        PangramBookRepositoryImpl(
            pangramBookNetworkDatasource: getIt<PangramRemoteDataSource>(),
            networkCheck: getIt<NetworkCheck>()))
    ..registerFactory<BookRepository>(() => PangramBookRepositoryImpl(
        pangramBookNetworkDatasource: getIt<PangramRemoteDataSource>(),
        networkCheck: getIt<NetworkCheck>()))
    ..registerLazySingleton<GetAllBookUsecase>(
        () => GetAllBookUsecase(bookRepository: getIt<BookRepository>()))
    ..registerLazySingleton<GetRandomPangramUsecase>(
        () => GetRandomPangramUsecase(bookRepository: getIt<BookRepository>()))
    ..registerFactory<PangramBloc>(() =>
        PangramBloc(getRandomPangramUsecase: getIt<GetRandomPangramUsecase>()));
}
