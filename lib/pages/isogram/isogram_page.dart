import 'package:dart_random_choice/dart_random_choice.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyIsogramPage extends StatefulWidget {
  @override
  _MyIsogramPageState createState() => _MyIsogramPageState();
}

class _MyIsogramPageState extends State<MyIsogramPage> {
  // final isogram = 'lumberjacks';
  final listIsogram = ['lumberjacks', 'background'];

  static const List<String> list = ['lumberjacks', 'listen'];
  var isogram = randomChoice<String>(list);

  @override
  Widget build(BuildContext context) {
    //Answer button
    Widget buildButton(Color color, String title, bool clicked) {
      return Material(
          elevation: 5.0,
          borderRadius: BorderRadius.circular(30.0),
          color: color,
          child: MaterialButton(
              minWidth: MediaQuery.of(context).size.width / 3,
              padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
              onPressed: () async {
                _checkIsogram(context, clicked);
              },
              child: Text(
                title,
                textAlign: TextAlign.center,
                style: GoogleFonts.lacquer(fontSize: 20, color: Colors.white),
              )));
    }

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Align(
            child: Container(
                padding: EdgeInsets.only(left: 10, right: 7, top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Icon(
                      Icons.games,
                      size: 25,
                      color: Colors.black87,
                    ),
                    Text(
                      'Isogram Checking',
                      style: GoogleFonts.lacquer(
                        fontSize: 28,
                      ),
                    ),
                    Icon(
                      Icons.games,
                      size: 25,
                      color: Colors.black87,
                    ),
                  ],
                )),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Keyword',
            style: GoogleFonts.lacquer(
              fontSize: 14,
            ),
          ),
          new Container(
            height: 50.0,
            width: 200,
            color: Colors.transparent,
            child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.black87,
                    borderRadius: new BorderRadius.all(Radius.circular(25))),
                child: new Center(
                  child: new Text(
                    isogram,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style:
                        GoogleFonts.lacquer(fontSize: 24, color: Colors.white),
                  ),
                )),
          ),
          SizedBox(
            height: 10,
          ),
          Text(
            'Is Isogram ?',
            style: GoogleFonts.lacquer(
              fontSize: 20,
            ),
          ),
          SizedBox(
            height: 10,
          ),
          new Container(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  buildButton(Colors.green, 'Yes', true),
                  buildButton(Colors.red, 'No', false)
                ]),
          ),
          SizedBox(
            height: 4,
          )
        ],
      ),
    );
  }

  // check button clicked YES / NO
  void _checkIsogram(BuildContext context, bool clicked) {
    var snackBar;
    if (clicked) {
      // Check isogram is in list of isogram
      if (listIsogram.contains(isogram)) {
        snackBar = SnackBar(
            content: Text(
          'Yes you\'re rigth',
          textAlign: TextAlign.center,
        ));
      } else {
        snackBar = SnackBar(
            content: Text(
          'Sorry it\'s wrong try again',
          textAlign: TextAlign.center,
        ));
      }
    } else {
      if (!listIsogram.contains(isogram)) {
        snackBar = SnackBar(
            content: Text(
          'Yes you\'re rigth',
          textAlign: TextAlign.center,
        ));
      } else {
        snackBar = SnackBar(
            content: Text(
          'Sorry it\'s wrong try again',
          textAlign: TextAlign.center,
        ));
      }
    }
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
