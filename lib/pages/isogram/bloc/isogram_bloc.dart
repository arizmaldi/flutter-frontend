import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'isogram_event.dart';
part 'isogram_state.dart';

class IsogramBloc extends Bloc<IsogramEvent, IsogramState> {
  @override
  IsogramState get initialState => IsogramInitial();

  @override
  Stream<IsogramState> mapEventToState(
    IsogramEvent event,
  ) async* {
    // TODO: Add Logic
  }
}
