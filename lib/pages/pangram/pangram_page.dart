import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pangram/common/config/injector.dart';
import 'package:flutter_pangram/common/network/pangram_client.dart';
import 'package:flutter_pangram/data/pangram_list/datasources/pangram_remote_datasource.dart';
import 'package:flutter_pangram/data/pangram_list/models/pangram_guttenberg.dart';
import 'package:flutter_pangram/pages/pangram/bloc/pangram_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:http/http.dart' as http;

class MyPangramPage extends StatefulWidget {
  MyPangramPage({Key key}) : super(key: key);

  @override
  _MyPangramPageState createState() => _MyPangramPageState();
}

class _MyPangramPageState extends State<MyPangramPage> {
  PangramGuttenberg _bookList;
  var items = List<PangramGuttenberg>();

  @override
  void initState() {
    super.initState();
  }

  PangramRemoteDataSource bookRDS = getIt<PangramRemoteDataSource>();

  Widget _buildPangramList() {
    if (_bookList == null) {
      return _buildProgressBar();
    } else {
      return GridView.builder(
        itemCount: 1,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          crossAxisCount: 1,
          childAspectRatio: MediaQuery.of(context).size.width /
              (MediaQuery.of(context).size.height / 1.33),
        ),
        itemBuilder: _buildItemList,
      );
    }
  }

  Widget _buildProgressBar() {
    return Center(child: CircularProgressIndicator());
  }

  Widget _buildItemList(BuildContext context, int index) {
    final book = _bookList;
    return Scaffold(
      body: Container(
          padding: const EdgeInsets.all(8),
          child: Card(
            elevation: 5,
            child: new InkWell(
              onTap: () {
                BlocProvider.of<PangramBloc>(context)
                    .add(PangramBookLoadEvent());
              },
              child: Column(
                children: <Widget>[
                  ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child: Image.network(
                      book.poster,
                      fit: BoxFit.fill,
                      height: 310,
                      width: 365,
                    ),
                  ),
                  ListTile(
                    title: Text(
                      book.title,
                      maxLines: 2,
                      overflow: TextOverflow.ellipsis,
                    ),
                    subtitle: Text(book.author),
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 12, right: 12),
                    child: Text(
                      book.pangram,
                      maxLines: 10,
                      textAlign: TextAlign.justify,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          )),
    );
  }

  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Align(
            child: Container(
                padding: EdgeInsets.only(left: 10, right: 7),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(
                      'Pangram Of The Day',
                      style: GoogleFonts.carterOne(fontSize: 24),
                    ),
                    Spacer(),
                    Text(
                      'SEE',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 15,
                          color: Colors.grey),
                    ),
                  ],
                )),
          ),
          Expanded(
            child: BlocConsumer<PangramBloc, PangramState>(
              builder: (context, state) {
                if (state is BookPangramDataState) {
                  _bookList = state.bookPangramList;
                  return _buildPangramList();
                } else {
                  return _buildProgressBar();
                }
              },
              listener: (context, state) {},
            ),
          ),
          SizedBox(
            height: 4,
          )
        ],
      ),
    );
  }
}
