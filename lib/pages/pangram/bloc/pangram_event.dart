part of 'pangram_bloc.dart';

@immutable
abstract class PangramEvent {}

@immutable
class PangramBookLoadEvent extends PangramEvent{}