import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:flutter_pangram/common/models/use_case.dart';
import 'package:flutter_pangram/domain/pangram_guttenberg/usecase/book_usecase.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'pangram_event.dart';
part 'pangram_state.dart';

@injectable
class PangramBloc extends Bloc<PangramEvent, PangramState> {
  final GetRandomPangramUsecase getRandomPangramUsecase;

  PangramBloc({this.getRandomPangramUsecase});

  @override
  PangramState get initialState => PangramInitialState();

  @override
  Stream<PangramState> mapEventToState(
    PangramEvent event,
  ) async* {
    if (event is PangramBookLoadEvent) {
      yield PangramLoadingState();
      try {
        final pangramList = await getRandomPangramUsecase(const NoPayload());
        yield BookPangramDataState(bookPangramList: pangramList);
      } catch (e) {
        yield BookPangramErrorState(e);
      }
    }
  }
}
