part of 'pangram_bloc.dart';

@immutable
abstract class PangramState {}

class PangramInitialState extends PangramState {}

class PangramLoadingState extends PangramState {}

@immutable
class BookPangramDataState extends PangramState {
  final bookPangramList;

  BookPangramDataState({this.bookPangramList});
}

@immutable
class BookPangramErrorState extends PangramState {
  final dynamic error;

  BookPangramErrorState(this.error);
}
