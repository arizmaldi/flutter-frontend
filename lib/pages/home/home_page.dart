import 'dart:io';

import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_pangram/common/config/injector.dart';
import 'package:flutter_pangram/pages/anagram/anagram_page.dart';
import 'package:flutter_pangram/pages/isogram/isogram_page.dart';
import 'package:flutter_pangram/pages/pangram/bloc/pangram_bloc.dart';
import 'package:flutter_pangram/pages/pangram/pangram_page.dart';
import 'package:google_fonts/google_fonts.dart';

enum HomePageOptions { anagram, pangram, isogram }

class MyHomePage extends StatefulWidget {
  final HomePageOptions page;

  const MyHomePage({
    Key key,
    this.page,
  }) : super(key: key);

  @override
  _MyHomeState createState() => _MyHomeState();
}

class _MyHomeState extends State<MyHomePage> {
  var _titleBar = 'Library';
  int _selectedPageIndex;

  @override
  void initState() {
    super.initState();
    _selectedPageIndex = widget.page?.index ?? 1;
  }

  final List<Widget> _pages = [
    MyAnagramPage(),
    MyPangramPage(),
    MyIsogramPage(),
  ];

  void _onItemSelected(int index) {
    setState(() {
      if (index == 0) {
        _titleBar = 'Games';
      } else if (index == 1) {
        _titleBar = 'Library';
      } else if (index == 2) {
        _titleBar = 'Games';
      }
      _selectedPageIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black87,
        title: Text(
          '$_titleBar',
          style: GoogleFonts.carterOne(fontSize: 24),
        ),
        centerTitle: true,
      ),
      body: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          color: Colors.white,
          child: BlocProvider<PangramBloc>(
            create: (context) =>
                getIt<PangramBloc>()..add(PangramBookLoadEvent()),
            child: _pages[_selectedPageIndex],
          )),
      bottomNavigationBar: CurvedNavigationBar(
        index: _selectedPageIndex,
        height: 45,
        color: Colors.black,
        buttonBackgroundColor: Colors.black,
        backgroundColor: Colors.transparent,
        items: const <Widget>[
          Icon(
            Icons.new_releases,
            size: 25,
            color: Colors.white,
          ),
          Icon(
            Icons.local_library,
            size: 25,
            color: Colors.white,
          ),
          Icon(
            Icons.new_releases,
            size: 25,
            color: Colors.white,
          ),
        ],
        onTap: _onItemSelected,
      ),
    );
  }
}
