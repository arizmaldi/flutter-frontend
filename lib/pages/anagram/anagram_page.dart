import 'dart:math';

import 'package:dart_random_choice/dart_random_choice.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class MyAnagramPage extends StatefulWidget {
  @override
  _MyAnagramPageState createState() => _MyAnagramPageState();
}

class _MyAnagramPageState extends State<MyAnagramPage> {
  final _formKey = GlobalKey<FormState>();
  // var anagram = 'listen';
  static const List<String> list = ['silent', 'listen'];
  var anagram = randomChoice<String>(list);
  final answer = ['silent', 'listen'];
  TextEditingController textFieldController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    //Answer field widget
    final answerField = Container(
        height: 50,
        width: 220,
        child: TextFormField(
          textAlign: TextAlign.center,
          controller: textFieldController,
          validator: (value) {
            if (value.isEmpty) {
              return 'Please enter some text';
            }
            return null;
          },
          obscureText: false,
          style: GoogleFonts.lacquer(fontSize: 24, color: Colors.black87),
          decoration: InputDecoration(
              contentPadding: EdgeInsets.fromLTRB(20.0, 5.0, 20.0, 15.0),
              hintText: "Anagram",
              hintStyle: TextStyle(color: Colors.grey),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(32.0))),
        ));

    //Check button
    final checkButton = Material(
        elevation: 5.0,
        borderRadius: BorderRadius.circular(30.0),
        color: Colors.black87,
        child: MaterialButton(
            minWidth: MediaQuery.of(context).size.width / 4,
            padding: EdgeInsets.fromLTRB(10.0, 5.0, 10.0, 5.0),
            onPressed: () async {
              _checkAnagram(context);
            },
            child: Text(
              "Check",
              textAlign: TextAlign.center,
              style: GoogleFonts.lacquer(fontSize: 20, color: Colors.white),
            )));

    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Align(
            child: Container(
                padding: EdgeInsets.only(left: 10, right: 7, top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Icon(
                      Icons.games,
                      size: 25,
                      color: Colors.black87,
                    ),
                    Text(
                      'Anagram Matching',
                      style: GoogleFonts.lacquer(
                        fontSize: 28,
                      ),
                    ),
                    Icon(
                      Icons.games,
                      size: 25,
                      color: Colors.black87,
                    ),
                  ],
                )),
          ),
          SizedBox(
            height: 20,
          ),
          Text(
            'Keyword',
            style: GoogleFonts.lacquer(
              fontSize: 14,
            ),
          ),
          new Container(
            height: 50.0,
            width: 200,
            color: Colors.transparent,
            child: new Container(
                decoration: new BoxDecoration(
                    color: Colors.black87,
                    borderRadius: new BorderRadius.all(Radius.circular(25))),
                child: new Center(
                  child: new Text(
                    anagram,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    style:
                        GoogleFonts.lacquer(fontSize: 24, color: Colors.white),
                  ),
                )),
          ),
          Text(
            'To',
            style: GoogleFonts.lacquer(
              fontSize: 14,
            ),
          ),
          answerField,
          SizedBox(
            height: 10,
          ),
          Form(
            key: _formKey,
            child: checkButton,
          ),
          SizedBox(
            height: 4,
          )
        ],
      ),
    );
  }

  void _checkAnagram(BuildContext context) {
    var snackBar;
    String jawab = textFieldController.text;
    if (_formKey.currentState.validate()) {
      if (answer.contains(jawab)) {
        if (anagram == jawab) {
          snackBar = SnackBar(
              content: Text(
            'Its same word!',
            textAlign: TextAlign.center,
          ));
        } else {
          snackBar = SnackBar(
              content: Text(
            'Yes you\'re rigth',
            textAlign: TextAlign.center,
          ));
        }
      } else {
        snackBar = SnackBar(
            content: Text(
          'Sorry it\'s wrong try again',
          textAlign: TextAlign.center,
        ));
      }
    }
    Scaffold.of(context).showSnackBar(snackBar);
  }
}
