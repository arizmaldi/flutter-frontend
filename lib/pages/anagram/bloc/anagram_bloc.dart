import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:meta/meta.dart';

part 'anagram_event.dart';
part 'anagram_state.dart';

class AnagramBloc extends Bloc<AnagramEvent, AnagramState> {
  @override
  AnagramState get initialState => AnagramInitial();

  @override
  Stream<AnagramState> mapEventToState(
    AnagramEvent event,
  ) async* {
    // TODO: Add Logic
  }
}
