import 'package:flutter_pangram/data/pangram_list/repositories/pangram_repository.dart';
import 'package:flutter_pangram/domain/pangram_guttenberg/entities/book_entity.dart';
import 'package:injectable/injectable.dart';

@Bind.toType(PangramBookRepositoryImpl)
@injectable
abstract class BookRepository {
  Future<BookGuttenbergEntity> getAllPangram();
  Future<BookGuttenbergEntity> getRandomPangram();
}
