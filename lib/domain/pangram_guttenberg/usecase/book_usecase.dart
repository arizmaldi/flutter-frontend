import 'package:flutter_pangram/common/models/use_case.dart';
import 'package:flutter_pangram/domain/pangram_guttenberg/entities/book_entity.dart';
import 'package:flutter_pangram/domain/pangram_guttenberg/repository/book_repository.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

@lazySingleton
@injectable
class GetAllBookUsecase implements UseCase<void, NoPayload> {
  final BookRepository bookRepository;

  GetAllBookUsecase({@required this.bookRepository});

  @override
  Future<BookGuttenbergEntity> call(NoPayload _) {
    return bookRepository.getAllPangram();
  }
}

@lazySingleton
@injectable
class GetRandomPangramUsecase implements UseCase<void, NoPayload> {
  final BookRepository bookRepository;

  GetRandomPangramUsecase({@required this.bookRepository});

  @override
  Future<BookGuttenbergEntity> call(NoPayload _) {
    return bookRepository.getRandomPangram();
  }
}
