import 'dart:convert';
import 'package:meta/meta.dart';

class BookGuttenbergEntity {
  final String id;
  final String title;
  final String author;
  final String poster;
  final String pangram;

  BookGuttenbergEntity({
    this.id,
    this.poster,
    @required this.title,
    @required this.author,
    this.pangram,
  });
}

extension BookGuttenbergEntityExt on BookGuttenbergEntity {
  BookGuttenbergEntity copyWith(
          {String id,
          String title,
          String author,
          String poster,
          List pangram}) =>
      BookGuttenbergEntity(
          id: id ?? this.id,
          title: title ?? this.title,
          author: author ?? this.author,
          poster: poster ?? this.poster,
          pangram: pangram ?? this.pangram);
}
